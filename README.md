drupal-vvvvjs
=============

This is a Drupal 7 module that will load all the necessary scripts to get vvvvjs on your site

http://www.vvvvjs.com/

http://vvvv.org


HOWTO
=============

This module enable your site to run vvvv patches as canvases inside a given div tag. The idea behind this module is to have a runtime only system. For real time patching in your browser you want to set up a non Drupal site as specified in http://www.vvvvjs.com/cheatsheet/index.html .

INSTALL
=============

Install the module as usual, you'll need to enable the Lbraries and the Jquery Update modules as well and set jquery to 1.8.2.

Download the vvvvjs library from http://www.vvvvjs.com/ to the library folder so that you have a folder called vvvvjs which contains the vvvvjs.js script. 